#####################################################################
##
##      Created 11/30/21 by ucdpadmin for cloud AWS. for aws
##
#####################################################################

variable "aws_instance_ami" {
  type = string
  description = "Generated"
  default = "ami-02edf5731752693cc"
}

variable "aws_instance_aws_instance_type" {
  type = string
  description = "Generated"
}

variable "availability_zone" {
  type = string
  description = "Generated"
}

variable "aws_instance_name" {
  type = string
  description = "Generated"
}

variable "aws_key_pair_name" {
  type = string
  description = "Generated"
}

